package com.ske.snakebaddesign.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by makham on 16/3/2559.
 */
public class DieCup {
    List<Die> listDie = new ArrayList<Die>();
    List<Integer> totalFace = new ArrayList<Integer>();
    private int numDice;
    private int numTotalFace;

    public DieCup(int numDice){
        this.numDice = numDice;
        createDie();
    }
    public DieCup(){
        this.numDice = 2;
        createDie();
    }
    public void createDie(){
        for (int i = 0 ; i < numDice ; i++){
            listDie.add(new Die());
        }
    }
    public void rollDieCup(){
        totalFace.clear();
        for(int i = 0 ; i < listDie.size() ; i++){
            listDie.get(i).rollDie();
            totalFace.add(listDie.get(i).getFace());
        }
    }
    public int getNumTotalFace(){
        this.numTotalFace = 0;
        for (int i = 0 ; i < totalFace.size() ; i++){
            numTotalFace += totalFace.get(i);
        }
        return numTotalFace;
    }
    public List<Integer> getTotalFace(){
       return totalFace;
    }

}
