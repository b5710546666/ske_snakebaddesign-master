package com.ske.snakebaddesign.model;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
/**
 * Created by makham on 16/3/2559.
 */
public class Game  extends Observable{
    List<Player> listPlayer = new ArrayList<Player>();
    private int numTotalFace;
    //
    private List<Integer> totalFace;
    private int numOfPlayer;
    private int boardSize;
    private DieCup diecup;
    private int turn;
    private int isGameOver = -1;

    public Game(int numOfPlayer,int boardSize){
        this.numOfPlayer = numOfPlayer;
        this.boardSize = boardSize;
        this.turn = 0;
        diecup = new DieCup();
    }

    public Game(){
        this.numOfPlayer = 2;
        this.boardSize = 6;
        this.turn = 0;
    }

    public void createPlayer(){
        for (int i = 0 ; i < numOfPlayer ; i++){
            listPlayer.add(new Player());
        }
    }

    public void resetGame(){
        this.setIsGameOver(-1);
        this.turn = 0;
        for (int i = 0 ; i < listPlayer.size() ; i++){
           listPlayer.get(i).playerReset();
        }
    }
    public void setNumdice(int numDice){
        this.diecup = new DieCup(numDice);
    }

    public void roll(){
        diecup.rollDieCup();
        numTotalFace = diecup.getNumTotalFace();
        //
        totalFace = diecup.getTotalFace();
        moveCurrentPiece();
    }

    public List<Player> getListPlayer(){
        return this.listPlayer;
    }

    public int getIsGameOver(){
        return isGameOver;
    }

    public void setIsGameOver(int i) { this.isGameOver = i; }

    public int getBoardSize(){return this.boardSize;}

    public int getNumTotalFace(){ return  this.numTotalFace; }

    //public List<Integer> getTotalFace(){ return  totalFace;}

    private int adjustPosition(Player player) {
        int current = player.getPlayerPosition();
        int distance = 0;
        distance+=numTotalFace;
        current = current+distance;
        int maxSquare = boardSize * boardSize - 1;
        //check if out of bound
        if(current > maxSquare) {
            current = maxSquare - (current - maxSquare);
        }
        return current;
    }

    private void moveCurrentPiece() {
        int playerNth = turn%numOfPlayer;
        if (turn%numOfPlayer == 0) {
            int adjusted = adjustPosition(listPlayer.get(0));
            listPlayer.get(0).setPosition(adjusted);
        } else {
            int adjusted = adjustPosition(listPlayer.get(playerNth));
            listPlayer.get(playerNth).setPosition(adjusted);
        }
        this.setChanged();
        checkWin();
        this.notifyObservers(listPlayer.get(playerNth));
        turn++;
    }

    public void checkWin(){
        for (int i = 0 ; i < numOfPlayer ; i++){
            if (getListPlayer().get(i).getPlayerPosition()==getBoardSize()*getBoardSize() - 1) {
                isGameOver = i;
                break;
            }
        }
    }
}
