package com.ske.snakebaddesign.model;

/**
 * Created by makham on 16/3/2559.
 */
public class Player {
    private Piece piece;
    private int number;
    private static int RUNNER =1;
    public Player(){
        this.number = RUNNER;
        RUNNER++;
        piece = new Piece();
    }
    public void playerReset(){
        this.piece.setPosition(0);
    }
    public int getPlayerPosition(){
        return piece.getPosition();
    }
    public void setPosition(int abjustPosition){
        this.piece.setPosition(abjustPosition);
    }
    public int getNumber() { return number; }
}
