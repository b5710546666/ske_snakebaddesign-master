package com.ske.snakebaddesign.model;

import java.util.Random;

/**
 * Created by makham on 16/3/2559.
 */
public class Die {
    private int face;
    public Die(){
        rollDie();
    }
    public void rollDie(){

        this.face = 1 + new Random().nextInt(6);

    }
    public int getFace(){
        return  this.face;
    }
}
