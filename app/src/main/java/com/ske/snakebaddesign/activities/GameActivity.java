package com.ske.snakebaddesign.activities;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ske.snakebaddesign.R;
import com.ske.snakebaddesign.guis.BoardView;
import com.ske.snakebaddesign.model.Game;
import com.ske.snakebaddesign.model.Player;

import java.util.Observable;
import java.util.Observer;

public class GameActivity extends AppCompatActivity implements Observer{
    private final Game game = new Game();
    private BoardView boardView;
    private Button buttonTakeTurn;
    private Button buttonRestart;
    private TextView textPlayerTurn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        initComponents();
    }
    @Override
    protected void onStart() {
        super.onStart();
        resetGame();
    }
    private void initComponents() {
        game.addObserver(this);
        setNumDie();
        game.createPlayer();
        boardView = (BoardView) findViewById(R.id.board_view);
        buttonTakeTurn = (Button) findViewById(R.id.button_take_turn);
        buttonTakeTurn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                game.roll();
                game.checkWin();

            }
        });
        buttonRestart = (Button) findViewById(R.id.button_restart);
        buttonRestart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                resetGame();
                setNumDie();
            }
        });
        textPlayerTurn = (TextView) findViewById(R.id.text_player_turn);
    }

    private void resetGame() {
        game.resetGame();
        boardView.setBoardSize(this.game.getBoardSize());
        boardView.setP1Position(game.getListPlayer().get(0).getPlayerPosition());
        boardView.setP2Position(game.getListPlayer().get(1).getPlayerPosition());
    }

    private void displayDialog(String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", listener);
        alertDialog.show();
    }

    public void checkGame(){
        if (game.getIsGameOver()!=-1) {
            String title = "Game Over";
            String msg = "";
            OnClickListener listener2 = new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    resetGame();
                    //dialog.dismiss();
                }
            };
            int winner = game.getIsGameOver()+1;
            msg = "Player " + winner + " won!";
            displayDialog(title, msg, listener2);
        }

        else {
            OnClickListener listener = new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };
//          displayDialog("Message", String.format("You got %d and %d", game.getTotalFace().get(0), game.getTotalFace().get(1)) , listener);
            displayDialog("Message", String.format("You got %d ", game.getNumTotalFace()) , listener);
        }

    }

    public void setNumDie(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Input amount of die: ");
        alertDialog.setMessage("Input number of die: ");
        alertDialog.setCancelable(false);

        final EditText input = new EditText(GameActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);

        alertDialog.setView(input);
        alertDialog.setPositiveButton("ok", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                int n = Integer.parseInt(input.getText().toString());
                game.setNumdice(n);
            }
        });
        alertDialog.show();

    }

    public void update(Observable observable, Object data) {
        final Player player = (Player)data;
        if(player.getNumber()==1) boardView.setP1Position(player.getPlayerPosition());
        else if(player.getNumber() == 2) boardView.setP2Position(player.getPlayerPosition());
        checkGame();
    }
}